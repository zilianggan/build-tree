#创建表
create table t_dict_define(
    id int(11) not null auto_increment COMMENT '主键id',
		define_name varchar(30) COMMENT '字典定义名字',
		define_value varchar(40) COMMENT '字典定义名字',
		parent_id int(11) COMMENT '逻辑外键指向当前表',
		PRIMARY key (id)
)engine=INNODB auto_increment=1 DEFAULT CHARSET=utf8;

create table t_dict_item(
    id int(11) not null auto_increment COMMENT '主键id',
		item_name varchar(30) COMMENT '字典项名字',
		item_value varchar(40) COMMENT '字典项值',
		define_id int(11) COMMENT '逻辑外键，关联字典定义表的id',
		PRIMARY key (id)
)engine=INNODB auto_increment=1 DEFAULT CHARSET=utf8;


show VARIABLES like '%log_bin%';

#开启允许创建函数
set GLOBAL log_bin_trust_function_creators = 1;

----------------------------------------------------------------------

#创建函数

#用于随机生成字符
delimiter $$
create FUNCTION rand_string(n int) returns VARCHAR(255)
begin
DECLARE chars_str VARCHAR(100) DEFAULT 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
DECLARE return_str VARCHAR(255) DEFAULT '';
DECLARE i int DEFAULT 0;
while i < n do
    #concat()是拼接字串，substring()是取字串【第二个入参是从哪个位置开始截取，第三个入参是指截取1个字符】，
		#floor()是取整，rand()是取[0,1)
    set return_str = concat(return_str, SUBSTRING(chars_str, floor(1+rand()*52), 1));
		set i = i + 1;
end while;
return return_str;
end $$

#用于随机生成多少到多少的编号
delimiter $$
create function rand_num(from_num int, to_num int) returns int(11)
begin
declare i int default 0;
set i = floor(from_num + rand()*(to_num-from_num+1));
return i;
end $$

#假如要删除
#drop function rand_num;

----------------------------------------------------------------------------

#创建存储过程

#插入t_dict_define数据
delimiter $$
create procedure insert_dict_define(max_num int)
begin
declare i int default 0;
set autocommit = 0;
repeat
    set i = i + 1;
		insert into t_dict_define(define_name, define_value, parent_id) values(rand_string(6), rand_string(6), rand_num(1, 50000));
		until i = max_num
end repeat;
commit;
end $$

#插入t_dict_item数据
delimiter $$
create procedure insert_dict_item(max_num int)
begin
declare i int default 0;
set autocommit = 0;
repeat
    set i = i + 1;
		insert into t_dict_item(item_name, item_value, define_id) values(rand_string(6), rand_string(6), rand_num(1, 10000));
		until i = max_num
end repeat;
commit;
end $$

-----------------------------------------------------------------------------------------------

#执行存储过程

#往dept表添加1万条数据
delimiter ;
call insert_dict_define(10000);

#往emp表添加50万条数据
call insert_dict_item(500000);
