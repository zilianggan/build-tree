package com.androidla.buildtree.basic.function;

import java.util.ArrayList;
import java.util.List;

public interface Treeable {

    List getChildren();

    void setChildren(List children);

}
