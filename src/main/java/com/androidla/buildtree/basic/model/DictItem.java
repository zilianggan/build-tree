package com.androidla.buildtree.basic.model;

import java.io.Serializable;
import lombok.Data;

/**
 * @Author Androidla
 * @Date 2020/12/17 01:04
 * @Description 字典项实体类
 */
@Data
public class DictItem implements Serializable {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 字典定义名字
     */
    private String defineName;

    /**
     * 字典定义名字
     */
    private String defineValue;

    /**
     * 逻辑外键指向当前表
     */
    private Integer parentId;

    private static final long serialVersionUID = 1L;
}