package com.androidla.buildtree.basic.model;

import java.io.Serializable;
import java.util.List;

import com.androidla.buildtree.basic.function.Treeable;
import lombok.Data;

/**
 * @Author Androidla
 * @Date 2020/12/17 01:04
 * @Description 字典定义实体类
 */
@Data
public class DictDefine implements Serializable, Treeable {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 字典定义名字
     */
    private String defineName;

    /**
     * 字典定义名字
     */
    private String defineValue;

    /**
     * 逻辑外键指向当前表
     */
    private Integer parentId;

    /**
     * 孩子节点
     */
    private List<DictDefine> children;

    private static final long serialVersionUID = 1L;

    @Override
    public List getChildren() {
        return children;
    }

    @Override
    public void setChildren(List children) {
        this.children = children;
    }
}