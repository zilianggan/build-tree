package com.androidla.buildtree.basic.mapper;

import com.androidla.buildtree.basic.model.DictDefine;

import java.util.List;

public interface DictDefineMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DictDefine record);

    int insertSelective(DictDefine record);

    DictDefine selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DictDefine record);

    int updateByPrimaryKey(DictDefine record);

    List<DictDefine> listAll();

}