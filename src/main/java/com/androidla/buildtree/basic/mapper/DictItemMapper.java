package com.androidla.buildtree.basic.mapper;

import com.androidla.buildtree.basic.model.DictItem;

public interface DictItemMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DictItem record);

    int insertSelective(DictItem record);

    DictItem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DictItem record);

    int updateByPrimaryKey(DictItem record);
}