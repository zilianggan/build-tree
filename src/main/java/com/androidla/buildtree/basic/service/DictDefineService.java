package com.androidla.buildtree.basic.service;

import com.androidla.buildtree.basic.mapper.DictDefineMapper;
import com.androidla.buildtree.basic.model.DictDefine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Androidla
 * @Date 2020/12/17 01:24
 **/
@Service
public class DictDefineService {

    @Autowired
    private DictDefineMapper mapper;

    public List<DictDefine> listAll() {
        List<DictDefine> list = mapper.listAll();
        return list;
    }
}
