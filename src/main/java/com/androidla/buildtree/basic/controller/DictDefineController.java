package com.androidla.buildtree.basic.controller;

import com.androidla.buildtree.basic.model.DictDefine;
import com.androidla.buildtree.basic.service.DictDefineService;
import com.androidla.buildtree.basic.util.TreeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author Androidla
 * @Date 2020/12/17 01:13
 **/
@RestController
@RequestMapping("/dict/define")
@Slf4j
public class DictDefineController {

    @Autowired
    private DictDefineService service;


    @GetMapping("/tree/linkedlist")
    public List<DictDefine> getTreeByLinkedList() {
        List<DictDefine> dictDefines = service.listAll();
        log.info("list size is: {}", dictDefines.size());
        List<DictDefine> result = TreeUtils.buildTreeByLinkedList(dictDefines, "FqQipc");
        log.info("result size is: {}", result.size());
        return result;
    }


    @GetMapping("/tree/arraylist")
    public List<DictDefine> getTreeByArrayList() {
        List<DictDefine> dictDefines = service.listAll();
        log.info("list size is: {}", dictDefines.size());
        List<DictDefine> result = TreeUtils.buildTreeByArrayList(dictDefines, "root");
        log.info("result size is: {}", result.size());
        return result;
    }
}
