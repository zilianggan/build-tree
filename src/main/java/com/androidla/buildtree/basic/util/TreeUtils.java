package com.androidla.buildtree.basic.util;

import com.androidla.buildtree.basic.model.DictDefine;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @Author Androidla
 * @Date 2020/12/17 01:45
 * @Description 关于树的工具类
 **/
@Slf4j
public class TreeUtils {

    /**
     * 默认的父节点
     */
    final static String PARENT_KEY = "root";



    public static List<DictDefine> buildTreeByLinkedList(List<DictDefine> dictDefines,
                                                        String parentKey) {

        /**
         * 1. 构建一个map，存储每个节点的孩子们
         * 2. 构建一个map，根据key可以获取到这个节点
         * 3. 根据传进来的字典定义名获取那个节点，用队列构建树
         */

        long startTime = System.currentTimeMillis();

        if (parentKey == null || parentKey.trim().length() == 0) {
            parentKey = PARENT_KEY;
        }

        // 构建一个map，存储每个节点的孩子节点
        Map<Integer, List<DictDefine>> parentIdChildrenMap = new HashMap<>();
        // 构建一个map，根据key可以获取到这个节点
        Map<String, DictDefine> defineNameDictDefineMap = new HashMap<>();
        for (DictDefine dictDefine : dictDefines) {
            Integer parentId = dictDefine.getParentId();
            List<DictDefine> dictDefineList = parentIdChildrenMap.get(parentId);
            if (dictDefineList == null) {
                dictDefineList = new ArrayList<>();
            }
            dictDefineList.add(dictDefine);
            parentIdChildrenMap.put(parentId, dictDefineList);
            defineNameDictDefineMap.put(dictDefine.getDefineName(), dictDefine);
        }

        // 根据传进来的字典定义名获取那个节点，用动态数组构建树
        DictDefine rootDictDefine = defineNameDictDefineMap.get(parentKey);

        if (rootDictDefine == null) {
            return new ArrayList<DictDefine>();
        }

        //用动态数组list构建树
        Queue<DictDefine> queue = new LinkedList<>();
        queue.offer(rootDictDefine);

        while (!queue.isEmpty() ) {
            DictDefine dictDefine = queue.poll();
            if (dictDefine == null) {
                continue;
            }
            List<DictDefine> childrenList = parentIdChildrenMap.get(dictDefine.getId());
            if (childrenList != null) {
                dictDefine.setChildren(childrenList);
                queue.addAll(childrenList);
            }

        }

        ArrayList<DictDefine> result = new ArrayList<>();
        result.add(rootDictDefine);

        long endTime = System.currentTimeMillis();

        log.info("It takes {}ms to deal it.", endTime-startTime);

        return result;
    }


    /**
     * 使用动态数组构建树
     * @param dictDefines
     * @param parentKey
     * @return
     */
    public static List<DictDefine> buildTreeByArrayList(List<DictDefine> dictDefines,
                                                    String parentKey) {

        /**
         * 1. 构建一个map，存储每个节点的孩子们
         * 2. 构建一个map，根据key可以获取到这个节点
         * 3. 根据传进来的字典定义名获取那个节点，用动态数组构建树
         */

        long startTime = System.currentTimeMillis();

        if (parentKey == null || parentKey.trim().length() == 0) {
            parentKey = PARENT_KEY;
        }

        // 构建一个map，存储每个节点的孩子节点
        Map<Integer, List<DictDefine>> parentIdChildrenMap = new HashMap<>();
        // 构建一个map，根据key可以获取到这个节点
        Map<String, DictDefine> defineNameDictDefineMap = new HashMap<>();
        for (DictDefine dictDefine : dictDefines) {
            Integer parentId = dictDefine.getParentId();
            List<DictDefine> dictDefineList = parentIdChildrenMap.get(parentId);
            if (dictDefineList == null) {
                dictDefineList = new ArrayList<>();
            }
            dictDefineList.add(dictDefine);
            parentIdChildrenMap.put(parentId, dictDefineList);
            defineNameDictDefineMap.put(dictDefine.getDefineName(), dictDefine);
        }

        // 根据传进来的字典定义名获取那个节点，用动态数组构建树
        DictDefine rootDictDefine = defineNameDictDefineMap.get(parentKey);

        if (rootDictDefine == null) {
            return new ArrayList<DictDefine>();
        }

        //用动态数组list构建树
        List<DictDefine> list = new ArrayList<>();
        list.add(rootDictDefine);

        int index = 0;
        while (!list.isEmpty() && index < list.size()) {
            DictDefine dictDefine = list.get(index);
            if (dictDefine == null) {
                continue;
            }
            List<DictDefine> childrenList = parentIdChildrenMap.get(dictDefine.getId());
            if (childrenList != null) {
                dictDefine.setChildren(childrenList);
                list.addAll(childrenList);
            }

            index++;
        }

        ArrayList<DictDefine> result = new ArrayList<>();
        result.add(rootDictDefine);

        long endTime = System.currentTimeMillis();

        log.info("It takes {}ms to deal it.", endTime-startTime);

        return result;
    }


}
