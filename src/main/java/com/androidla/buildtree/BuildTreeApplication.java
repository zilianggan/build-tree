package com.androidla.buildtree;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan(basePackages = "com.androidla.buildtree.*.mapper")
@SpringBootApplication
public class BuildTreeApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuildTreeApplication.class, args);
    }

}
