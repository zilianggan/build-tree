package com.androidla.buildtree;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.*;

/**
 * @Author Androidla
 * @Date 2021/1/4 14:35
 * @Description 遍历二叉树的算法
 **/
@Slf4j
public class TraverseBinaryTree {

    List<Integer> preOrderList = new ArrayList<>();
    List<Integer> inOrderList = new ArrayList<>();
    List<Integer> afterOrderList = new ArrayList<>();

    // 二叉树的节点
    private static class TreeNode {
        int val = 0;
        TreeNode left = null;
        TreeNode right = null;

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        TreeNode(int val) {
            this.val = val;
        }
    }

    /**
     * 前序遍历
     * @param node
     * @return
     */
    public List<Integer> preOrderTraverse(TreeNode node) {
        if (node == null) {
            return preOrderList;
        }
        preOrderList.add(node.val);
        preOrderTraverse(node.left);
        preOrderTraverse(node.right);
        return preOrderList;
    }

    /**
     * 中序遍历
     * @param node
     * @return
     */
    public List<Integer> inOrderTraverse(TreeNode node) {
        if (node == null) {
            return inOrderList;
        }
        inOrderTraverse(node.left);
        inOrderList.add(node.val);
        inOrderTraverse(node.right);
        return inOrderList;
    }

    /**
     * 后序遍历
     * @param node
     * @return
     */
    public List<Integer> afterOrderTraverse(TreeNode node) {
        if (node == null) {
            return afterOrderList;
        }
        afterOrderTraverse(node.left);
        afterOrderTraverse(node.right);
        afterOrderList.add(node.val);
        return afterOrderList;
    }

    /**
     * 层次遍历二叉树
     * @param node
     * @return 返回的格式为[[], [], []]
     */
    public List<ArrayList<Integer>> levelTraverse(TreeNode node) {
        if (node == null) {
            return new ArrayList<>();
        }

        List<ArrayList<Integer>> result = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();

        // 加入队列
        queue.offer(node);

        while (!queue.isEmpty()) {
            // 返回结果的格式弄成[[], [], []]，因此要获取size
            int size = queue.size();
            ArrayList<Integer> list = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                // 出队列
                TreeNode curNode = queue.poll();
                if (curNode == null) continue;

                // 入队
                if (curNode.left != null) queue.offer(curNode.left);
                if (curNode.right != null) queue.offer(curNode.right);

                list.add(curNode.val);
            }
            result.add(list);
        }

        return result;
    }

    /**
     * 深度优先遍历
     * @param node
     * @return
     */
    public Stack<Integer> depthTraverse(TreeNode node) {
        if (node == null) {
            return new Stack<>();
        }

        Stack<Integer> result = new Stack<>();
        Stack<TreeNode> stack = new Stack<>();

        stack.push(node);

        while (!stack.isEmpty()) {
            TreeNode curNode = stack.pop();

            if (curNode == null) continue;

            // 先遍历树的左边，后遍历树的右边，栈是先进后出
            if (curNode.right != null) stack.push(curNode.right);
            if (curNode.left != null) stack.push(curNode.left);

            result.push(curNode.val);
        }

        return result;
    }

    /**
     * 测试前中后序遍历
     */
    @Test
    public void testPreInAfterOrderTraverse() {
        TreeNode node1 = new TreeNode(1, null, null);
        TreeNode node2 = new TreeNode(2, null, null);
        TreeNode node3 = new TreeNode(3, null, null);
        TreeNode node4 = new TreeNode(4, null, null);
        TreeNode node5 = new TreeNode(5, node4, node3);
        TreeNode node6 = new TreeNode(6, node2, node1);
        TreeNode node7 = new TreeNode(7, node6, node5);
        List<Integer> preOrder = preOrderTraverse(node7);
        List<Integer> inOrder = inOrderTraverse(node7);
        List<Integer> afterOrder = afterOrderTraverse(node7);
        log.info("preOrderList is: {}", preOrder);
        log.info("inOrderList is : {}", inOrder);
        log.info("afterOrderList is: {}", afterOrder);
    }

    /**
     * 测试层次遍历
     */
    @Test
    public void testlevelTraverse() {
        TreeNode node1 = new TreeNode(1, null, null);
        TreeNode node2 = new TreeNode(2, null, null);
        TreeNode node3 = new TreeNode(3, null, null);
        TreeNode node4 = new TreeNode(4, null, null);
        TreeNode node5 = new TreeNode(5, node4, node3);
        TreeNode node6 = new TreeNode(6, node2, node1);
        TreeNode node7 = new TreeNode(7, node6, node5);
        List<ArrayList<Integer>> result = levelTraverse(node7);
        log.info("result is: {}", result);
    }

    /**
     * 测试深度遍历
     */
    @Test
    public void testDepthTraverse() {
        TreeNode node1 = new TreeNode(1, null, null);
        TreeNode node2 = new TreeNode(2, null, null);
        TreeNode node3 = new TreeNode(3, null, null);
        TreeNode node4 = new TreeNode(4, null, null);
        TreeNode node5 = new TreeNode(5, node4, node3);
        TreeNode node6 = new TreeNode(6, node2, node1);
        TreeNode node7 = new TreeNode(7, node6, node5);
        Stack<Integer> result = depthTraverse(node7);

        while (!result.isEmpty()) {
            Integer value = result.pop();
            log.info("{}", value);
        }
        log.info("result is: {}", result);
    }


}
