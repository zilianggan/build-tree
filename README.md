# 一. 工程简介
本工程旨在实现构建树状菜单栏返回给前端。详情可前往https://blog.csdn.net/qq_40634846/article/details/111317490查看。

Springboot：2.3 \
MySQL：8.0 \
Jdk: 1.8

# 二. 部署介绍
在src/main/resource/sql文件夹下面，有一个create_data.sql脚本文件。

在数据库创建名为dictionary数据库,在此数据库执行create_data.sql里面的内容

至此即可完成部署。

# 三. 归档记录

- 2021年1月4日 更新遍历二叉树的各种算法。源码在test/java/com/androidla/buildtree/TraverseBinaryTree

